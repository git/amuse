package accounts

type User struct {
	Username string
	IsAdmin  bool
	Session  string
	Timezone string
}

func (u User) IsEmpty() bool {
	return u.Username == ""
}

type Authentication struct {
	Token     string
	Necessary bool
}

type AuthError struct {
	Err error
}

func (e AuthError) Error() string {
	return "Auth error: " + e.Err.Error()
}

func (e AuthError) Unwrap() error {
	return e.Err
}
