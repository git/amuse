package datastructure

import (
	"notabug.org/apiote/amuse/i18n"

	"strings"
	"time"
)

type Source struct {
	Url  string
	Name string
}

type Work interface {
	GetArticle() string
	SetDescription(description string)
}

type Book struct {
	Id           string
	Uri          string
	Source       []Source
	Cover        string
	Authors      []string
	Year         int64
	Title        string
	SerieUri     string
	SerieName    string
	PartInSerie  string
	Genres       []string
	Article      string
	Description  string
	IsOnWantList bool
	Experiences  []time.Time
}

func (b Book) GetArticle() string {
	return b.Article
}

func (b *Book) SetDescription(description string) {
	if b.Description == "" {
		b.Description = description
	}
}

func (b *Book) GetItemInfo() ItemInfo {
	return ItemInfo{
		Cover:     b.Cover,
		Title:     b.Title,
		YearStart: int(b.Year),
		Genres:    strings.Join(b.Genres, ", "),
	}
}

func (b *Book) GetItemType() ItemType {
	return ItemTypeBook
}

func (b *Book) SetOnWantList(isOnList bool) {
	b.IsOnWantList = isOnList
}

func (b Book) GetLastExperienceFull(strings i18n.Translation) string {
	return i18n.FormatDate(b.Experiences[0], strings.Global["date_format_full"], strings.Global)
}

func (b Book) GetLastExperience(strings i18n.Translation, timezone string) string {
	return i18n.FormatDateNice(b.Experiences[0], strings, timezone)
}
