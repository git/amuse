package datastructure

type ValueError struct {
	Message string
}

func (e ValueError) Error() string {
	return e.Message
}
