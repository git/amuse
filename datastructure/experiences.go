package datastructure

import (
	"notabug.org/apiote/amuse/i18n"

	"time"
)

type ExperiencesEntry struct {
	ItemInfo
	Type     string
	Id       string
	Code     string
	Datetime time.Time
}

type Experiences struct {
	List  []ExperiencesEntry
	Page  int
	Pages int
	Query string
}

func (e ExperiencesEntry) FormatDatetime(strings i18n.Translation) string {
	return i18n.FormatDate(e.Datetime, strings.Global["date_format_time"], strings.Global)
}

func (e Experiences) NextPage() int {
	if e.Page < e.Pages {
		return e.Page + 1
	} else {
		return e.Page
	}
}

func (e Experiences) PrevPage() int {
	if e.Page > 1 {
		return e.Page - 1
	} else {
		return e.Page
	}
}
