package datastructure

import (
	"strconv"
	"strings"
)

type ItemInfo struct {
	Cover      string
	Status     string
	Title      string
	YearStart  int
	YearEnd    int
	BasedOn    string
	Genres     string
	Runtime    int
	Collection int
	Part       int
	Episodes   int
}

func (i ItemInfo) IsUnreleased(itemType ItemType) bool {
	return (itemType == ItemTypeFilm && i.Status != "Released") ||
		(itemType == ItemTypeTvserie && i.Status != "Returning Series" && i.Status != "Ended")
}

func (i ItemInfo) GetGenres(genres map[int]string) string {
	genreIds := strings.Split(i.Genres, ",")
	genreNames := []string{}
	for _, genreId := range genreIds {
		if genreId != "" {
			genreIdInt, _ := strconv.ParseInt(genreId, 10, 64)
			if genres[int(genreIdInt)] != "" {
				genreNames = append(genreNames, genres[int(genreIdInt)])
			}
		}
	}
	return strings.Join(genreNames, ", ")
}

type Item interface {
	GetItemInfo() ItemInfo
	GetItemType() ItemType
	SetOnWantList(isOnList bool)
}

type ItemType string

const (
	ItemTypeBook    ItemType = "book"
	ItemTypeFilm             = "film"
	ItemTypeTvserie          = "tvserie"
	ItemTypeUnkown           = "unknown"
)
