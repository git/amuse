package datastructure

type List interface {
	SetGenres(map[int]string)
	GetType() ItemType
}
