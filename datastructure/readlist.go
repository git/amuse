package datastructure

type ReadlistEntry struct {
	ItemInfo
	Id          string
	HasPrevious bool
}

type Readlist struct {
	List   []ReadlistEntry
	Page   int
	Pages  int
	Genres map[int]string
	Query  string
}

func (w *Readlist) SetGenres(m map[int]string) {
	w.Genres = m
}

func (w *Readlist) GetType() ItemType {
	return ItemTypeBook
}

func (w Readlist) NextPage() int {
	if w.Page < w.Pages {
		return w.Page + 1
	} else {
		return w.Page
	}
}

func (w Readlist) PrevPage() int {
	if w.Page > 1 {
		return w.Page - 1
	} else {
		return w.Page
	}
}
