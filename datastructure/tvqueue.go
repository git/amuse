package datastructure

import (
	"strconv"
)

func min(a, b int) int {
	if a > b {
		return b
	} else {
		return a
	}
} // todo replicated code from libamuse/serie.go

type TvQueueEntry struct {
	ItemInfo
	Id              string
	HasPrevious     bool
	WatchedEpisodes int
	SkippedEpisodes int
}

func (e TvQueueEntry) GetYears() string {
	if e.YearStart == 0 {
		return ""
	} else if e.Status == "Ended" || e.Status == "Canceled" {
		if e.YearEnd == e.YearStart || e.YearEnd == 0 {
			return strconv.FormatInt(int64(e.YearStart), 10)
		} else {
			return strconv.FormatInt(int64(e.YearStart), 10) + "–" + strconv.FormatInt(int64(e.YearEnd), 10)
		}
	} else {
		return strconv.FormatInt(int64(e.YearStart), 10) + "–"
	}
}

func (e TvQueueEntry) CalculateProgress() int { // todo replicated code from libamuse/serie.go
	if e.Episodes-e.SkippedEpisodes == 0 {
		return 0
	}
	return min(e.WatchedEpisodes*100/(e.Episodes-e.SkippedEpisodes), 100)
}

type TvQueue struct {
	List   []TvQueueEntry
	Page   int
	Pages  int
	Genres map[int]string
	Query  string
}

func (q *TvQueue) SetGenres(m map[int]string) {
	q.Genres = m
}

func (q *TvQueue) GetType() ItemType {
	return ItemTypeTvserie
}

func (q TvQueue) NextPage() int {
	if q.Page < q.Pages {
		return q.Page + 1
	} else {
		return q.Page
	}
}

func (q TvQueue) PrevPage() int {
	if q.Page > 1 {
		return q.Page - 1
	} else {
		return q.Page
	}
}
