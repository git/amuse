package datastructure

type WatchlistEntry struct {
	ItemInfo
	Id          string
	HasPrevious bool
}

type Watchlist struct {
	List   []WatchlistEntry
	Page   int
	Pages  int
	Genres map[int]string
	Query  string
}

func (w *Watchlist) SetGenres(m map[int]string) {
	w.Genres = m
}

func (w *Watchlist) GetType() ItemType {
	return ItemTypeFilm
}

func (w Watchlist) NextPage() int {
	if w.Page < w.Pages {
		return w.Page + 1
	} else {
		return w.Page
	}
}

func (w Watchlist) PrevPage() int {
	if w.Page > 1 {
		return w.Page - 1
	} else {
		return w.Page
	}
}
