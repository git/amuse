package front

import (
	"notabug.org/apiote/amuse/tmdb"
	"notabug.org/apiote/amuse/wikidata"
	"notabug.org/apiote/amuse/datastructure"

	"golang.org/x/text/language"

	"github.com/pquerna/otp"
)

func TODO(message string) interface{} {
	panic(message)
}

type CapnprotoRenderer struct{}

func (CapnprotoRenderer) RenderFilm(film *tmdb.Film, languages []language.Tag) string {
	return TODO("implement CapnprotoRenderer.RenderFilm").(string)
}

func (CapnprotoRenderer) RenderSearch(results *tmdb.SearchResults, inventaireResults *wikidata.SearchResults, languages []language.Tag) string {
	return TODO("implement CapnprotoRenderer.RenderSearch").(string)
}

func (CapnprotoRenderer) RenderIndex(randomComedy string, languages []language.Tag) string {
	return TODO("implement CapnprotoRenderer.RenderIndex").(string)
}

func (CapnprotoRenderer) RenderTvSerie(serie *tmdb.TvSerie, languages []language.Tag) string {
	return TODO("implement CapnprotoRenderer.RenderSerie").(string)
}

func (CapnprotoRenderer) RenderPerson(person *tmdb.Person, languages []language.Tag) string {
	return TODO("implement CapnprotoRenderer.RenderPerson").(string)
}

func (CapnprotoRenderer) RenderBook(book datastructure.Book, languages []language.Tag) string {
	return TODO("implement CapnprotoRenderer.RenderBook").(string)
}
func (CapnprotoRenderer) RenderBookSerie(bookSerie wikidata.BookSerie, languages []language.Tag) string {
	return TODO("implement CapnprotoRenderer.RenderBookSerie").(string)
}

func (CapnprotoRenderer) RenderAbout(languages []language.Tag) string {
	return TODO("implement CapnprotoRenderer.RenderAbout").(string)
}

func (CapnprotoRenderer) RenderErrorPage(code int, languages []language.Tag) string {
	return TODO("implement CapnprotoRenderer.RenderErrorPage").(string)
}

func (CapnprotoRenderer) RenderLogin(languages []language.Tag, err error, target string) string {
	// todo throw Wrong Accept
	return TODO("implement CapnprotoRenderer.RenderLogin").(string)
}

func (CapnprotoRenderer) RenderLoggedOut(languages []language.Tag) string {
	// todo throw Wrong Accept
	return TODO("implement CapnprotoRenderer.RenderLogin").(string)
}

func (CapnprotoRenderer) RenderSignup(languages []language.Tag, err error, otp *otp.Key, sfaEnabled bool, username, qr string) string {
	// todo throw Wrong Accept
	return TODO("implement CapnprotoRenderer.RenderSignup").(string)
}

func (CapnprotoRenderer) RenderSignedup(languages []language.Tag, recoveryCodes []string) string {
	// todo throw Wrong Accept
	return TODO("implement CapnprotoRenderer.RenderSignedup").(string)
}

func (CapnprotoRenderer) RenderWatchlist(watchlist datastructure.Watchlist, languages []language.Tag) string {
	return TODO("implement CapnprotoRenderer.RenderWatchlist").(string)
}

func (CapnprotoRenderer) RenderTvQueue(watchlist datastructure.TvQueue, languages []language.Tag) string {
	return TODO("implement CapnprotoRenderer.RenderTvQueue").(string)
}

func (CapnprotoRenderer) RenderReadlist(readlist datastructure.Readlist, languages []language.Tag) string {
	return TODO("implement CapnprotoRenderer.RenderWatchlist").(string)
}

func (CapnprotoRenderer) RenderExperiences(experiences datastructure.Experiences, languages []language.Tag) string {
	return TODO("implement CapnprotoRenderer.RenderExperiences").(string)
}
