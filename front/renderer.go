package front

import (
	"notabug.org/apiote/amuse/accounts"
	"notabug.org/apiote/amuse/datastructure"
	"notabug.org/apiote/amuse/tmdb"
	"notabug.org/apiote/amuse/wikidata"

	"golang.org/x/text/language"

	"github.com/pquerna/otp"
)

type NoSuchRendererError struct {
	mimetype string
}

func (e NoSuchRendererError) Error() string {
	return "No renderer for " + e.mimetype
}

type Renderer interface {
	RenderFilm(*tmdb.Film, []language.Tag) string
	RenderSearch(*tmdb.SearchResults, *wikidata.SearchResults, []language.Tag) string
	RenderIndex(string, []language.Tag) string
	RenderTvSerie(*tmdb.TvSerie, []language.Tag) string
	RenderPerson(*tmdb.Person, []language.Tag) string
	RenderBook(datastructure.Book, []language.Tag) string
	RenderBookSerie(wikidata.BookSerie, []language.Tag) string
	RenderAbout([]language.Tag) string
	RenderErrorPage(int, []language.Tag) string
	RenderLogin([]language.Tag, error, string) string
	RenderLoggedOut([]language.Tag) string
	RenderSignup([]language.Tag, error, *otp.Key, bool, string, string) string
	RenderSignedup([]language.Tag, []string) string
	RenderWatchlist(datastructure.Watchlist, []language.Tag) string
	RenderTvQueue(datastructure.TvQueue, []language.Tag) string
	RenderReadlist(datastructure.Readlist, []language.Tag) string
	RenderExperiences(datastructure.Experiences, []language.Tag) string
}

func NewRenderer(mimetype string, user accounts.User) (Renderer, error) {
	switch mimetype {
	case "text/html":
		return HtmlRenderer{user: user}, nil
	case "application/capnproto":
		return CapnprotoRenderer{}, nil
	default:
		return nil, NoSuchRendererError{}
	}
}
