module notabug.org/apiote/amuse

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/alecthomas/chroma v0.7.2 // indirect
	github.com/bytesparadise/libasciidoc v0.4.0
	github.com/chai2010/webp v1.1.0
	github.com/dlclark/regexp2 v1.2.0 // indirect
	github.com/knakk/digest v0.0.0-20160404164910-fd45becddc49 // indirect
	github.com/knakk/rdf v0.0.0-20190304171630-8521bf4c5042 // indirect
	github.com/knakk/sparql v0.0.0-20191213045353-fd0bd0e76475
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pquerna/otp v1.2.0
	github.com/sirupsen/logrus v1.5.0 // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	golang.org/x/crypto v0.0.0-20200423211502-4bdfaf469ed5
	golang.org/x/sys v0.0.0-20200420163511-1957bb5e6d1f // indirect
	golang.org/x/text v0.3.2
	golang.org/x/tools v0.0.0-20200423205358-59e73619c742 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
	notabug.org/apiote/gott v1.1.2
)
