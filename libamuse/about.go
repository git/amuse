package libamuse

import (
	"notabug.org/apiote/amuse/accounts"
	
	"notabug.org/apiote/gott"
)

func renderAbout(args ...interface{}) interface{} {
	result := args[1].(*Result)
	languages := result.languages
	renderer := result.renderer
	result.page = renderer.RenderAbout(languages)
	return gott.Tuple(args)
}

func ShowAbout(language, mimetype string, auth accounts.Authentication) (string, error) {
	auth.Necessary = false
	r, err := gott.
		NewResult(gott.Tuple{&RequestData{language: language, mimetype: mimetype, auth: auth}, &Result{}}).
		Bind(parseLanguage).
		Bind(verifyToken).
		Bind(createRenderer).
		Map(renderAbout).
		Finish()

	if err != nil {
		return "", err
	} else {
		return r.(gott.Tuple)[1].(*Result).page, nil
	}
}
