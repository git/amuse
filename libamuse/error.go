package libamuse

import (
	"notabug.org/apiote/gott"
)

func renderErrorPage(args ...interface{}) interface{} {
	data := args[0].(*RequestData)
	result := args[1].(*Result)
	code := data.code
	result.page = result.renderer.RenderErrorPage(code, result.languages)
	return gott.Tuple(args)
}

func ShowErrorPage(code int, language, mimetype string) (string, error) {
	r, err := gott.
		NewResult(gott.Tuple{&RequestData{id: "", language: language, mimetype: mimetype, code: code}, &Result{}}).
		Bind(parseLanguage).
		Bind(createRenderer).
		Map(renderErrorPage).
		Finish()

	if err != nil {
		return "", err
	} else {
		return r.(gott.Tuple)[1].(*Result).page, nil
	}
}
