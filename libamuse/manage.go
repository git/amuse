package libamuse

import (
	"notabug.org/apiote/amuse/accounts"
	"notabug.org/apiote/amuse/datastructure"
	"notabug.org/apiote/amuse/db"

	"strings"

	"fmt"
)

func MakeAdmin(username string) error {
	return db.MakeAdmin(username)
}

func TouchWantlist() error {
	uris, err := db.GetWantlistUris()
	if err != nil {
		return err
	}
	auth := accounts.Authentication{Token: ""}
	for _, uri := range uris {
		fmt.Printf("Touching %s\n", uri)
		type_id := strings.Split(uri, "/")
		itemType := datastructure.ItemType(type_id[0])
		itemId := type_id[1]
		switch itemType {
		case datastructure.ItemTypeFilm:
			_, _ = ShowFilm(itemId, "en-GB", "text/html", auth)
		case datastructure.ItemTypeTvserie:
			_, _ = ShowTvSerie(itemId, "", "en-GB", "text/html", auth)
		}

	}

	return nil
}
