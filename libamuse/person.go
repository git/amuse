package libamuse

import (
	"notabug.org/apiote/amuse/tmdb"
	"notabug.org/apiote/amuse/accounts"

	"notabug.org/apiote/gott"
)

func getPerson(args ...interface{}) (interface{}, error) {
	data := args[0].(*RequestData)
	result := args[1].(*Result)
	languages := result.languages
	person, err := tmdb.GetPerson(data.id, languages[0].String(), "")
	result.result = person
	return gott.Tuple(args), err
}

func renderPerson(args ...interface{}) interface{} {
	result := args[1].(*Result)
	person := result.result.(*tmdb.Person)
	result.page = result.renderer.RenderPerson(person, result.languages)
	return gott.Tuple(args)
}

func ShowPerson(id, etag, language, mimetype string, auth accounts.Authentication) (string, error) {
	auth.Necessary = false
	r, err := gott.
		NewResult(gott.Tuple{&RequestData{id: id,language: language, mimetype: mimetype, auth: auth}, &Result{}}).
		Bind(parseLanguage).
		Bind(verifyToken).
		Bind(getPerson).
		Bind(createRenderer).
		Map(renderPerson).
		Finish()

	if err != nil {
		return "", err
	} else {
		return r.(gott.Tuple)[1].(*Result).page, nil
	}
}
