package main

import (
	"notabug.org/apiote/amuse/config"
	"notabug.org/apiote/amuse/db"
	"notabug.org/apiote/amuse/libamuse"

	"flag"
	"fmt"
	"os"
	"log"
)

func main() {
	port := flag.Int("p", -1, "port to run amuse on")
	address := flag.String("a", "", "address to run amuse on")
	dataHome := flag.String("d", "", "data directory")
	manage := flag.String("m", "", "manage command")
	configPath := flag.String("c", "/etc/amuse.toml", "configPath")
	flag.Parse()

	config.ReadConfig(*configPath)
	if *dataHome != "" {
		config.DataHome = *dataHome
	}
	if *port > 0 {
		config.Port = uint(*port)
	}
	if *address != "" {
		config.Address = *address
	}

	validateState()

	db.Migrate()

	switch *manage {
	case "makeadmin":
		var username string
		fmt.Printf("Username: ")
		fmt.Scanf("%s", &username)
		err := libamuse.MakeAdmin(username)
		if err != nil {
			os.Exit(1)
		} else {
			return
		}
	case "touch":
		err := libamuse.TouchWantlist()
		if err != nil {
			os.Exit(1)
		} else {
			return
		}
	}

	route(config.Port)
}

func validateState() {
	_, err := os.Readlink(config.DataHome + "/i18n/default.toml")
	if err != nil {
		log.Println("WARN: i18n/default.toml is not a symbolic link; translations fallback will not work")
	}
	if config.TmdbApiKey == "" {
		log.Fatalln("ERR: TmdbApiKey not specified in config")
	}
}
