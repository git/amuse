ALL=`echo static/img/*.svg | sed 's/svg/webp/g'` amuse

all:V: $ALL
reallyall:V: $ALL pymodule

amuse: main.go router.go go.mod go.sum `echo front/*.go i18n/*.go libamuse/*.go tmdb/*.go wikidata/*.go inventaire/*.go db/*.go datastructure/*.go network/*.go accounts/*.go`
	go build -ldflags "-s -w -linkmode external -extldflags -static"
static/img/%.webp: static/img/%.svg
	rendersvg static/img/$stem.svg static/img/$stem.png
	cwebp -lossless static/img/$stem.png -o static/img/$stem.webp
	rm static/img/$stem.png

PREFIX=`echo ${PREFIX:-/usr/local}`
install:V: amuse
	mkdir -m755 -p $PREFIX/bin
	mkdir -m755 -p $PREFIX/share/amuse
	mkdir -m755 -p $PREFIX/share/amuse/templates
	mkdir -m755 -p $PREFIX/share/amuse/i18n
	mkdir -m755 -p $PREFIX/share/amuse/static/style
	mkdir -m755 -p $PREFIX/share/amuse/static/img
	mkdir -m755 -p $PREFIX/share/amuse/static/fonts
	install -m755 amuse $PREFIX/bin/amuse
	install -m644 templates/*.html $PREFIX/share/amuse/templates/
	install -m644 i18n/*-*.toml $PREFIX/share/amuse/i18n/
	ln -s $PREFIX/share/amuse/i18n/en-GB.toml $PREFIX/share/amuse/i18n/default.toml
	install -m644 static/style/style.css $PREFIX/share/amuse/static/style/style.css
	install -m644 static/img/*.webp $PREFIX/share/amuse/static/img/
	install -m644 static/fonts/MaterialIcons-Regular.woff2 $PREFIX/share/amuse/static/fonts/MaterialIcons-Regular.woff2

uninstall:V:
	rm $PREFIX/bin/amuse
	rm -r $PREFIX/share/amuse

zip:V: amuse.tgz
amuse.tgz: amuse `echo templates/* static/* i18n/*`
	tar czf amuse.tgz templates i18n/??-??.toml static amuse
