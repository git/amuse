package tmdb

import (
	"notabug.org/apiote/amuse/datastructure"
	"notabug.org/apiote/amuse/db"
	"notabug.org/apiote/amuse/network"

	"notabug.org/apiote/gott"
)

type Show interface {
	AddBasedOn(book datastructure.Book)
}

type ShowCastEntry struct {
	Character    string
	Id           int
	Name         string
	Profile_path string `json:"profile_path"`
}

type ShowCrewEntry struct {
	Job          string
	Id           int
	Name         string
	Profile_path string `json:"profile_path"`
}

type ShowCredits struct {
	Cast []ShowCastEntry
	Crew []ShowCrewEntry
}

func GetItemTypeFromShow(show Show) datastructure.ItemType {
	if _, ok := show.(*Film); ok {
		return datastructure.ItemTypeFilm
	} else if _, ok := show.(*TvSerie); ok {
		return datastructure.ItemTypeTvserie
	} else {
		return datastructure.ItemTypeUnkown
	}
}

func getCacheEntry(args ...interface{}) (interface{}, error) {
	result := args[1].(*network.Result)
	uri := result.Request.URL.String()
	entry, err := db.GetCacheEntry(uri)
	if err != nil || entry == nil {
		return gott.Tuple(args), err
	}

	result.Etag = entry.Etag
	result.Body = entry.Data
	return gott.Tuple(args), nil
}

func cleanCache(args ...interface{}) error {
	err := db.CleanCache()
	return err
}

func saveCacheEntry(args ...interface{}) error {
	result := args[1].(*network.Result)
	uri := result.Request.URL.String()
	err := db.SaveCacheEntry(uri, result.Etag, result.Body)
	return err
}

